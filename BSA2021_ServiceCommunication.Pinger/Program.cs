﻿using Microsoft.Extensions.Configuration;
using RabbitMQ.Wrapper;
using RabbitMQ.Wrapper.Interfaces;
using System;
using System.IO;
using System.Reflection;

namespace BSA2021_ServiceCommunication.Pinger
{
    class Program
    {
        static void Main(string[] args)
        {
            IConfiguration config = GetConfig();
            string rabbitServerUri = config["rabbitServerUri"];
            using (IPingPongService item = new PingPongService(TypePingPong.Ping, rabbitServerUri))
            {
                item.SendMessageToQueue();
                item.ListenQueue();
                Console.ReadKey();
            }
        }

        static IConfiguration GetConfig()
        {
            FileInfo file = new FileInfo($@"{Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location)}\config.json");
            if (!file.Exists)
                throw new Exception("Config file doesn't exist");
            var builder = new ConfigurationBuilder()
                    .AddJsonFile(file.FullName, optional: false);
            return builder.Build();
        }
    }
}
