﻿using Newtonsoft.Json;
using RabbitMQ.Client;
using RabbitMQ.Wrapper.Interfaces;
using RabbitMQ.Wrapper.Models;
using RabbitMQ.Wrapper.QueueServices;
using System;
using System.Text;
using System.Threading;

namespace RabbitMQ.Wrapper
{
    public enum TypePingPong: byte
    {
        Ping = 0,
        Pong
    }
    public class PingPongService: IPingPongService
    {
        private readonly IMessageProducerScope _messageProducerDirectScope;
        private readonly IMessageConsumerScope _messageConsumerDirectScope;

        private readonly IMessageProducerScope _messageProducerFanoutScope;
        private readonly IMessageConsumerScope _messageConsumerFanoutScope;

        private readonly string _sender;
        private readonly string _receiver;
        private readonly string[] _prefixes = { "Powerful", "Forceful", "Impressive", "Mighty", "Vigorous", "Almighty", "Effectual", "Strengthy" };

        public PingPongService(TypePingPong typePingPong, string rabbitServerUri)
        {
            IConnectionFactory connectionFactory = new ConnectionFactory { Uri = new Uri(rabbitServerUri) };
            IMessageProducerScopeFactory producerFactory = new MessageProducerScopeFactory(connectionFactory);
            IMessageConsumerScopeFactory consumerFactory = new MessageConsumerScopeFactory(connectionFactory);

            if (typePingPong == TypePingPong.Ping)
            {
                _sender = TypePingPong.Ping.ToString().ToLower();
                _receiver = TypePingPong.Pong.ToString().ToLower();
            }
            if (typePingPong == TypePingPong.Pong)
            {
                _sender = TypePingPong.Pong.ToString().ToLower();
                _receiver = TypePingPong.Ping.ToString().ToLower();
            }

            Console.WriteLine($"< {_sender.ToUpper()} >");

            _messageProducerDirectScope = producerFactory.Open(new MessageScopeSettings
            {
                ExchangeName = "PingPongExchange",
                ExchangeType = ExchangeType.Direct,
                QueueName = _receiver + "_queue",
                RoutingKey = _receiver + "Key"
            });

            _messageConsumerDirectScope = consumerFactory.Open(new MessageScopeSettings
            {
                ExchangeName = "PingPongExchange",
                ExchangeType = ExchangeType.Direct,
                QueueName = _sender + "_queue",
                RoutingKey = _sender + "Key"
            });

            _messageProducerFanoutScope = producerFactory.Open(new MessageScopeSettings
            {
                ExchangeName = "PingPongWinnerBroadcastExchange",
                ExchangeType = ExchangeType.Fanout,
                RoutingKey = ""
            });

            // when we set QueueName = "auto", will be created a non-durable, exclusive, autodelete queue with a generated name
            _messageConsumerFanoutScope = consumerFactory.Connect(new MessageScopeSettings
            {
                ExchangeName = "PingPongWinnerBroadcastExchange",
                ExchangeType = ExchangeType.Fanout,
                QueueName = "auto",
                RoutingKey = ""
            });
            _messageConsumerFanoutScope.MessageConsumer.Received += (sender, args) =>
            {
                var body = args.Body;
                string message = Encoding.UTF8.GetString(body.ToArray());
                PingPongGameModel game = JsonConvert.DeserializeObject<PingPongGameModel>(message);

                Console.BackgroundColor = ConsoleColor.Yellow;
                Console.ForegroundColor = ConsoleColor.Black;
                Console.WriteLine($"\n {game.Message} WINNER !!!  Game ID: {game.Id} ");
                Console.BackgroundColor = ConsoleColor.Black;
                Console.ForegroundColor = ConsoleColor.Gray;

                _messageConsumerFanoutScope.MessageConsumer.SetAcknowledge(args.DeliveryTag, true);
            };
        }


        public void ListenQueue()
        {
            _messageConsumerDirectScope.MessageConsumer.Connect();
            _messageConsumerDirectScope.MessageConsumer.Received += (sender, args) =>
            {
                var body = args.Body;
                string message = Encoding.UTF8.GetString(body.ToArray());
                PingPongGameModel game = JsonConvert.DeserializeObject<PingPongGameModel>(message);

                Console.WriteLine($"\n {game.Message,-18} Time: {DateTime.Now.ToString("HH:mm:ss.fff")} {"", -4}Game ID: {game.Id} ");

                // The probability of one side not missing the ball is 85%.
                double probability = new Random().NextDouble();
                if (probability <= 0.85)
                {                  
                    Thread.Sleep(2500);
                    SendMessageToQueue(game.Id);
                }
                else
                {
                    _messageProducerFanoutScope.MessageProducer.Send(message);
                }
                
                _messageConsumerDirectScope.MessageConsumer.SetAcknowledge(args.DeliveryTag, true);
            };
        }
        public void SendMessageToQueue(int? gameID = null)
        {
            PingPongGameModel game = new PingPongGameModel();
            if (gameID is null)
                game.Id = new Random().Next(0, int.MaxValue);
            else
                game.Id = gameID.Value;
            string prefix = _prefixes[new Random().Next(0, _prefixes.Length)];
            game.Message = $"{prefix} {_sender}";
            _messageProducerDirectScope.MessageProducer.Send(JsonConvert.SerializeObject(game));
        }


        public void Dispose()
        {
            _messageProducerDirectScope?.Dispose();
            _messageConsumerDirectScope?.Dispose();
            _messageProducerFanoutScope?.Dispose();
            _messageConsumerFanoutScope?.Dispose();
        }
    }
}
