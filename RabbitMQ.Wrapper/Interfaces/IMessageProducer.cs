﻿using System;

namespace RabbitMQ.Wrapper.Interfaces
{
    internal interface IMessageProducer
    {
        void Send(string message, string type = null);
        void SendType(Type type, string message);
    }
}
