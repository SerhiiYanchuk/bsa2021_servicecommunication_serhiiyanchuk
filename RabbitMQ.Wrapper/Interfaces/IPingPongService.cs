﻿using RabbitMQ.Wrapper.Models;
using System;

namespace RabbitMQ.Wrapper.Interfaces
{
    public interface IPingPongService: IDisposable
    {
        void ListenQueue();
        void SendMessageToQueue(int? gameId = null);
    }
}
