﻿using System;

namespace RabbitMQ.Wrapper.Interfaces
{
    internal interface IMessageProducerScope: IDisposable
    {
        IMessageProducer MessageProducer { get; }
    }
}
