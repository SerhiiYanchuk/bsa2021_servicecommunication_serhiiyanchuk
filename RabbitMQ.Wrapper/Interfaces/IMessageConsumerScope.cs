﻿using System;

namespace RabbitMQ.Wrapper.Interfaces
{
    internal interface IMessageConsumerScope: IDisposable
    {
        public IMessageConsumer MessageConsumer { get; }
    }
}
