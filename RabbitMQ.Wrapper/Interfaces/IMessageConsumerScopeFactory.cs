﻿using RabbitMQ.Wrapper.Models;

namespace RabbitMQ.Wrapper.Interfaces
{
    internal interface IMessageConsumerScopeFactory
    {
        IMessageConsumerScope Open(MessageScopeSettings messageScopeSettings);
        IMessageConsumerScope Connect(MessageScopeSettings messageScopeSettings);
    }
}
