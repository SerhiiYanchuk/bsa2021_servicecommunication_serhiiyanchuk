﻿using RabbitMQ.Client.Events;
using System;

namespace RabbitMQ.Wrapper.Interfaces
{
    internal interface IMessageConsumer
    {
        event EventHandler<BasicDeliverEventArgs> Received;
        void Connect();
        void SetAcknowledge(ulong deliveryTag, bool processed);

    }    
}
