﻿using RabbitMQ.Client;
using System;

namespace RabbitMQ.Wrapper.Interfaces
{
    internal interface IMessageQueue: IDisposable
    {
        IModel Channel { get; }
        void DeclareExchange(string exchangeName, string exchangeType);
        void BindQueue(string queueName, string exchangeName, string routingKey);
        string BindQueue(string exchangeName, string routingKey);
    }
}
