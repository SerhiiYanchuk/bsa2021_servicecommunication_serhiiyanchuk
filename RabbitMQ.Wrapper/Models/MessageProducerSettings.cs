﻿using RabbitMQ.Client;

namespace RabbitMQ.Wrapper.Models
{
    internal class MessageProducerSettings
    {
        public IModel Channel { get; set; }
        public PublicationAddress PublicationAddress { get; set; }
    }
}
