﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RabbitMQ.Wrapper.Models
{
    internal class PingPongGameModel
    {
        public int Id { get; set; }
        public string Message { get; set; }
    }
}
