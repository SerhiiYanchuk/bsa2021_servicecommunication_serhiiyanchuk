﻿using RabbitMQ.Client;

namespace RabbitMQ.Wrapper.Models
{
    internal class MessageConsumerSettings
    {
        public bool SequentialFetch { get; set; } = true;
        public bool AutoAcknowledge { get; set; } = false;
        public IModel Channel { get; set; }
        public string QueueName { get; set; }
    }
}
