﻿using RabbitMQ.Client;
using RabbitMQ.Wrapper.Interfaces;
using RabbitMQ.Wrapper.Models;


namespace RabbitMQ.Wrapper.QueueServices
{
    internal class MessageQueue: IMessageQueue
    {
        private readonly IConnection _connection;
        public MessageQueue(IConnectionFactory connectionFactory)
        {
            _connection = connectionFactory.CreateConnection();
            Channel = _connection.CreateModel();
        }
        public MessageQueue(IConnectionFactory connectionFactory, MessageScopeSettings messageScopeSettings) : this(connectionFactory)
        {
            DeclareExchange(messageScopeSettings.ExchangeName, messageScopeSettings.ExchangeType);
            if (messageScopeSettings.QueueName == "auto")
            {
                messageScopeSettings.QueueName = BindQueue(messageScopeSettings.ExchangeName, messageScopeSettings.RoutingKey);
            }
            else if (messageScopeSettings.QueueName is not null)
            {
                BindQueue(messageScopeSettings.QueueName, messageScopeSettings.ExchangeName, messageScopeSettings.RoutingKey);
            }
        }
        public IModel Channel { get; protected set; }
        public void DeclareExchange(string exchangeName, string exchangeType)
        {
            Channel.ExchangeDeclare(exchangeName, exchangeType ?? string.Empty);
        }
        public void BindQueue(string queueName, string exchangeName, string routingKey)
        {
            Channel.QueueDeclare(queueName, true, false, false);
            Channel.QueueBind(queueName, exchangeName, routingKey);
        }
        public string BindQueue(string exchangeName, string routingKey)
        {
            // when we supply no parameters to QueueDeclare() we create a non-durable, exclusive, autodelete queue with a generated name
            var queueName = Channel.QueueDeclare().QueueName;
            Channel.QueueBind(queueName, exchangeName, "");
            return queueName;
        }
        public void Dispose()
        {
            Channel?.Dispose();
            _connection?.Dispose();
        }
    }
}
